package com.example.android.xtivity.Sports;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.android.xtivity.AdapterMisc.AppData;
import com.example.android.xtivity.AdapterMisc.TutorialAdapter;
import com.example.android.xtivity.Model.Tutorial;
import com.example.android.xtivity.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TutorialActivity extends AppCompatActivity {

    AppData appData;
    DatabaseReference databaseReference;
    RecyclerView rvTutorial;
    TutorialAdapter tutorialAdapter;
    List<Tutorial> tutorialList = new ArrayList<>();
    TextView emptyQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        emptyQuery = findViewById(R.id.emptyQuery);
        rvTutorial = findViewById(R.id.rvTutorial);
        rvTutorial.setLayoutManager(new LinearLayoutManager(this));
        tutorialAdapter = new TutorialAdapter(this, tutorialList);
        rvTutorial.setAdapter(tutorialAdapter);

        appData = new AppData(this);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("tutorial").orderByChild("filterTag").equalTo(appData.getStringData(AppData.SPORT)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    emptyQuery.setVisibility(View.GONE);
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        tutorialList.add(ds.getValue(Tutorial.class));
                    }
                    tutorialAdapter.notifyDataSetChanged();
                } else {
                    emptyQuery.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
