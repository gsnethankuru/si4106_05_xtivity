package com.example.android.xtivity.AdapterMisc;

import com.example.android.xtivity.Model.Arena;
import com.example.android.xtivity.Model.Event;
import com.example.android.xtivity.Model.News;
import com.example.android.xtivity.Model.Product;
import com.example.android.xtivity.Model.Trainer;
import com.example.android.xtivity.Model.Tutorial;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

// helper class buat ngisi DB
public class TestData {

    // fungsi statis buat ngisi DB
    public static void generate() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

        /**
         * Format data:
         * List<Data> dataList = new Arraylist<>() -> list isinya objek data yg akan dimasukin ke DB
         * dataList.add(new Data(...)); -> nambahin objek data baru, yg ... itu isi dari objek yg ditambahin. Ini buat nambahin/ngedit data yg ada
         * for (Data data : dataList) {
         *             databaseReference.child("data").child(data.getUid()).setValue(data); -> for loop buat nambahin semua data objek di list ke DB
         *         }
         */

        List<Arena> arenaList = new ArrayList<>();
        arenaList.add(new Arena(
                "abc",
                "Motion Skatepark",
                "Kuta, Badung",
                "Jl. Sunset Road No.420, Kuta, Kabupaten Badung, Bali 80361",
                "Bali",
                "https://firebasestorage.googleapis.com/v0/b/xtivity-c3129.appspot.com/o/arena%2FMotion%20Skatepark%202.png?alt=media&token=419e3474-29f9-4c34-82c7-96b29e2b5a31",
                4.3,
                "Bali"
        ));

        arenaList.add(new Arena(
                "abd",
                "Taman Pasopati Skatepark",
                "Kota Bandung",
                "Taman jomblo, Tamansari, Kec. Bandung Wetan, Kota Bandung, Jawa Barat 40116",
                "Bandung",
                "https://firebasestorage.googleapis.com/v0/b/xtivity-c3129.appspot.com/o/arena%2Ftaman%20pasoepati%20skatepark%202.png?alt=media&token=b36d1d0d-c561-4195-a7ed-12a34fe4d930",
                4,
                "skateboard_Bandung"
        ));

        arenaList.add(new Arena(
                "abe",
                "Amplitude Skatepark",
                "Canggu, Badung",
                "Jl. Pelajar Pejuang, Canggu, Kabupaten Badung, Bali",
                "Bali",
                "https://firebasestorage.googleapis.com/v0/b/xtivity-c3129.appspot.com/o/arena%2Famplitude.jpg?alt=media&token=4013e4c6-a16b-400c-95db-e82198fd23f8",
                4.5,
                "skateboard_Bali"
        ));

        for (Arena arena : arenaList) {
            databaseReference.child("arena").child(arena.getUid()).setValue(arena);
            databaseReference.child("location").child(arena.getLocation()).setValue(true);
        }

        List<Trainer> trainerList = new ArrayList<>();
        trainerList.add(new Trainer(
                "abc",
                "Pevi Permana",
                "Luckyline Skatepark, Bandung",
                "+62 81999078987",
                "https://firebasestorage.googleapis.com/v0/b/xtivity-c3129.appspot.com/o/trainer%2Fpevi%20permana%202%201.png?alt=media&token=b653ee98-6961-4ef3-84e9-2db4e41dbe38",
                "skateboard"
        ));

        trainerList.add(new Trainer(
                "abd",
                "Mario Palandeng",
                "Donkey Skate School, Bali",
                "+62 81999078987",
                "https://firebasestorage.googleapis.com/v0/b/xtivity-c3129.appspot.com/o/trainer%2Fmario%20palandeng%201.png?alt=media&token=c5762d91-ad4a-49b5-93de-52b5e137d4fa",
                "skateboard"
        ));


        for (Trainer trainer : trainerList) {
            databaseReference.child("trainer").child(trainer.getUid()).setValue(trainer);
        }

        List<Event> eventList = new ArrayList<>();
        eventList.add(new Event(
                "abc",
                "Indonesia Open X-SPORTS Championship",
                "Karanganyar, Central Java",
                "25 - 27 October 2019",
                "www.ioxc.co.id",
                "https://firebasestorage.googleapis.com/v0/b/xtivity-c3129.appspot.com/o/event%2FIOXC%201.png?alt=media&token=f9fba68e-2dac-4efb-bfe3-d3371836a0e9",
                "skateboard"
        ));

        eventList.add(new Event(
                "abd",
                "PRO JAM Skateboard & BMX Comp",
                "Sentul Waterpark, Cilacap",
                "1 April 2019",
                "www.projam.id",
                "https://firebasestorage.googleapis.com/v0/b/xtivity-c3129.appspot.com/o/event%2FPro%20Jam%20Skate%20Comp%201.png?alt=media&token=f4beb19d-a777-4e7f-a9b1-941c98e09009",
                "skateboard"
        ));

        for (Event event : eventList) {
            databaseReference.child("event").child(event.getUid()).setValue(event);
        }

        List<Tutorial> tutorialList = new ArrayList<>();
        tutorialList.add(new Tutorial(
                "abc",
                "John Krasinki",
                "https://firebasestorage.googleapis.com/v0/b/xtivity-c3129.appspot.com/o/tutorial%2Fabc.mp4?alt=media&token=8909a90c-5652-421c-9db6-fea7a887b447",
                "skateboard"
        ));

        tutorialList.add(new Tutorial(
                "abd",
                "Fox w/ Camera",
                "https://firebasestorage.googleapis.com/v0/b/xtivity-c3129.appspot.com/o/tutorial%2Fabd.mp4?alt=media&token=4d53dd4f-5a0d-4f0f-a0ef-5a03c9588a07",
                "skateboard"
        ));

        for (Tutorial tutorial : tutorialList) {
            databaseReference.child("tutorial").child(tutorial.getUid()).setValue(tutorial);
        }

        List<Product> productList = new ArrayList<>();
        productList.add(new Product(
                "abc",
                "Deck Skate - SOVRN",
                450999,
                5,
                "https://firebasestorage.googleapis.com/v0/b/xtivity-c3129.appspot.com/o/product%2FIMG_3511_600x%201.png?alt=media&token=8dd5c948-358a-4bc6-ab76-1fb048cf99f8"
        ));

        productList.add(new Product(
                "abd",
                "BMX Cult 2019",
                1450999,
                0,
                "https://firebasestorage.googleapis.com/v0/b/xtivity-c3129.appspot.com/o/product%2Fbmx%202.png?alt=media&token=cac90942-b41b-4af8-a530-efc745d2776c"
        ));

        productList.add(new Product(
                "abe",
                "Macroblade Inline Shoes",
                850999,
                1,
                "https://firebasestorage.googleapis.com/v0/b/xtivity-c3129.appspot.com/o/product%2Frolerblade%201.png?alt=media&token=44734550-acd5-42a8-bcb4-a134175335d0"
        ));

        productList.add(new Product(
                "abf",
                "Spitfire Griptape",
                50999,
                0,
                "https://firebasestorage.googleapis.com/v0/b/xtivity-c3129.appspot.com/o/product%2Fmob_Spitfire_Flames_large%201.png?alt=media&token=61610e03-a204-4f60-931e-27acdaf7c44a"
        ));

        for (Product product : productList) {
            databaseReference.child("product").child(product.getUid()).setValue(product);
        }

        List<News> newsList = new ArrayList<>();
        newsList.add(new News(
                "abc",
                "Sanggoe Dharma Meraih Peringkat ke 2 di ajang Asian Games pekan lalu.",
                "\t\tSanggoe Dharma Tanjung, 16 tahun, memimpin kualifikasi nomor street skateboard Asian Games 2018 dan berpeluang meraih medali emas di final. \n\n\t\tSanggoe tampil impresif di babak kualifikasi dan tidak terpengaruh oleh gangguan hujan yang sempat dua kali menghentikan pertandingan di Jakabaring Sport City, Palembang, Selasa, 28 Agustus 2018. \n\n\t\tSkateborder asal Bali ini berada di peringkat pertama dengan nilai 31,3 poin. Ia unggul hampir tiga poin dari Keyoki Ike asal Jepang, yang mengumpulkan 28,3 poin. \n\n\t\tSanggoe mengawali sesi babak (runs) 1 dengan nilai 6,4 dan runs 2 melejit menjadi 8,1 poin. Ia terlihat menguasai hampir semua bagian arena terutama rintangan rel, salah satunya dengan trik bs smithgrid menuruni rel delapan anak tangga. \n\n\t\tDominasi Sanggoe makin terlihat di sesi trik terbaik. Ia berhasil mengeksekusi lima kali kesempatan tanpa terjatuh. \n\n\t\tSanggoe bahkan mencatatkan dua nilai tinggi, pertama dengan trik bigflip fs boardslide yang meraih 7,7 poin. Kemudian trik bigflip fs boardslide yang mendapat 7,8 poin dari dewan juri. \n\n\t\tSanggoe Dharma merupakan peringkat ketiga pada ajang FISE World Malaysia 2014, peringkat enam dunia di X-Games Shanghai 2015, dan skateboarder Indonesia pertama yang berlaga di Street League (SLS) di Barcelona 2017. \n\n\t\tIndonesia mengunggulkan Sanggoe untuk bisa meraih emas di cabang skatebord. \"Kita memang sudah memprediksi dia (Sanggoe) bisa dapat nilai tinggi, dan meraih emas. Tinggal kita menjaga mentalnya supaya tampil bagus di final, kata pelatih skateboard Indonesia, Ardy Polli. Indonesia menurunkan dua atlet untuk nomor ini, yakni Sanggoe Dharma Tanjung dan Aldwin Angkawidjaja. Keduanya lolos ke final yang akan dipertandingkan Rabu besok, 29 Agustus 2018. Aldwin berada pada peringkat tujuh klasemen kualifikasi. \n\n\t\tAda delapan skateboarder yang akan memperebutkan medali emas pada nomor ini. Saingan paling berat dari Indonesia datang dari skateborder Jepang, Keyoki Ike. \n\n\t\tCabang olahraga skatebord baru pertama kali ini dipertandingkan di Asian Games, yang menjadi persiapan untuk Olimpiade 2020 di Tokyo, Jepang",
                "https://firebasestorage.googleapis.com/v0/b/xtivity-c3129.appspot.com/o/news%2FSanggoe%20Juara%202.png?alt=media&token=404198e0-0ca0-4c60-aebe-14381f806b67"
        ));

        newsList.add(new News(
                "abd",
                "Sanggoe Dharma Meraih Peringkat ke 2 di ajang Asian Games pekan lalu.",
                "\t\tSanggoe Dharma Tanjung, 16 tahun, memimpin kualifikasi nomor street skateboard Asian Games 2018 dan berpeluang meraih medali emas di final. \n\n\t\tSanggoe tampil impresif di babak kualifikasi dan tidak terpengaruh oleh gangguan hujan yang sempat dua kali menghentikan pertandingan di Jakabaring Sport City, Palembang, Selasa, 28 Agustus 2018. \n\n\t\tSkateborder asal Bali ini berada di peringkat pertama dengan nilai 31,3 poin. Ia unggul hampir tiga poin dari Keyoki Ike asal Jepang, yang mengumpulkan 28,3 poin. \n\n\t\tSanggoe mengawali sesi babak (runs) 1 dengan nilai 6,4 dan runs 2 melejit menjadi 8,1 poin. Ia terlihat menguasai hampir semua bagian arena terutama rintangan rel, salah satunya dengan trik bs smithgrid menuruni rel delapan anak tangga. \n\n\t\tDominasi Sanggoe makin terlihat di sesi trik terbaik. Ia berhasil mengeksekusi lima kali kesempatan tanpa terjatuh. \n\n\t\tSanggoe bahkan mencatatkan dua nilai tinggi, pertama dengan trik bigflip fs boardslide yang meraih 7,7 poin. Kemudian trik bigflip fs boardslide yang mendapat 7,8 poin dari dewan juri. \n\n\t\tSanggoe Dharma merupakan peringkat ketiga pada ajang FISE World Malaysia 2014, peringkat enam dunia di X-Games Shanghai 2015, dan skateboarder Indonesia pertama yang berlaga di Street League (SLS) di Barcelona 2017. \n\n\t\tIndonesia mengunggulkan Sanggoe untuk bisa meraih emas di cabang skatebord. \"Kita memang sudah memprediksi dia (Sanggoe) bisa dapat nilai tinggi, dan meraih emas. Tinggal kita menjaga mentalnya supaya tampil bagus di final, kata pelatih skateboard Indonesia, Ardy Polli. Indonesia menurunkan dua atlet untuk nomor ini, yakni Sanggoe Dharma Tanjung dan Aldwin Angkawidjaja. Keduanya lolos ke final yang akan dipertandingkan Rabu besok, 29 Agustus 2018. Aldwin berada pada peringkat tujuh klasemen kualifikasi. \n\n\t\tAda delapan skateboarder yang akan memperebutkan medali emas pada nomor ini. Saingan paling berat dari Indonesia datang dari skateborder Jepang, Keyoki Ike. \n\n\t\tCabang olahraga skatebord baru pertama kali ini dipertandingkan di Asian Games, yang menjadi persiapan untuk Olimpiade 2020 di Tokyo, Jepang",
                "https://firebasestorage.googleapis.com/v0/b/xtivity-c3129.appspot.com/o/news%2F123213.png?alt=media&token=42d5838e-fc93-4dd4-8c2a-9e4487f3052e"
        ));

        newsList.add(new News(
                "abe",
                "Sanggoe Dharma Meraih Peringkat ke 2 di ajang Asian Games pekan lalu.",
                "\t\tSanggoe Dharma Tanjung, 16 tahun, memimpin kualifikasi nomor street skateboard Asian Games 2018 dan berpeluang meraih medali emas di final. \n\n\t\tSanggoe tampil impresif di babak kualifikasi dan tidak terpengaruh oleh gangguan hujan yang sempat dua kali menghentikan pertandingan di Jakabaring Sport City, Palembang, Selasa, 28 Agustus 2018. \n\n\t\tSkateborder asal Bali ini berada di peringkat pertama dengan nilai 31,3 poin. Ia unggul hampir tiga poin dari Keyoki Ike asal Jepang, yang mengumpulkan 28,3 poin. \n\n\t\tSanggoe mengawali sesi babak (runs) 1 dengan nilai 6,4 dan runs 2 melejit menjadi 8,1 poin. Ia terlihat menguasai hampir semua bagian arena terutama rintangan rel, salah satunya dengan trik bs smithgrid menuruni rel delapan anak tangga. \n\n\t\tDominasi Sanggoe makin terlihat di sesi trik terbaik. Ia berhasil mengeksekusi lima kali kesempatan tanpa terjatuh. \n\n\t\tSanggoe bahkan mencatatkan dua nilai tinggi, pertama dengan trik bigflip fs boardslide yang meraih 7,7 poin. Kemudian trik bigflip fs boardslide yang mendapat 7,8 poin dari dewan juri. \n\n\t\tSanggoe Dharma merupakan peringkat ketiga pada ajang FISE World Malaysia 2014, peringkat enam dunia di X-Games Shanghai 2015, dan skateboarder Indonesia pertama yang berlaga di Street League (SLS) di Barcelona 2017. \n\n\t\tIndonesia mengunggulkan Sanggoe untuk bisa meraih emas di cabang skatebord. \"Kita memang sudah memprediksi dia (Sanggoe) bisa dapat nilai tinggi, dan meraih emas. Tinggal kita menjaga mentalnya supaya tampil bagus di final, kata pelatih skateboard Indonesia, Ardy Polli. Indonesia menurunkan dua atlet untuk nomor ini, yakni Sanggoe Dharma Tanjung dan Aldwin Angkawidjaja. Keduanya lolos ke final yang akan dipertandingkan Rabu besok, 29 Agustus 2018. Aldwin berada pada peringkat tujuh klasemen kualifikasi. \n\n\t\tAda delapan skateboarder yang akan memperebutkan medali emas pada nomor ini. Saingan paling berat dari Indonesia datang dari skateborder Jepang, Keyoki Ike. \n\n\t\tCabang olahraga skatebord baru pertama kali ini dipertandingkan di Asian Games, yang menjadi persiapan untuk Olimpiade 2020 di Tokyo, Jepang",
                "https://firebasestorage.googleapis.com/v0/b/xtivity-c3129.appspot.com/o/news%2Fasdsad.png?alt=media&token=d803aead-3cba-41e6-9739-c63bbbe3b601"
        ));

        for (News news : newsList) {
            databaseReference.child("news").child(news.getUid()).setValue(news);
        }
    }
}
