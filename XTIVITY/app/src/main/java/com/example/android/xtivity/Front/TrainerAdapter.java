package com.example.android.xtivity.AdapterMisc;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.xtivity.Model.Trainer;
import com.example.android.xtivity.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class TrainerAdapter extends RecyclerView.Adapter<TrainerAdapter.TrainerViewHolder> {

    Context context;
    List<Trainer> trainerList;

    public TrainerAdapter(Context context, List<Trainer> trainerList) {
        this.context = context;
        this.trainerList = trainerList;
    }

    @NonNull
    @Override
    public TrainerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.card_trainer, parent, false);
        return new TrainerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TrainerViewHolder holder, int position) {
        Trainer trainer = trainerList.get(position);
        Glide.with(context)
                .load(trainer.getImageUrl())
                .into(holder.trainerImage);
        holder.trainerName.setText(trainer.getName());
        holder.trainerLocation.setText(trainer.getLocation());
        holder.trainerPhone.setText(trainer.getPhone());
    }

    @Override
    public int getItemCount() {
        return trainerList.size();
    }

    public class TrainerViewHolder extends RecyclerView.ViewHolder {
        ImageView trainerImage;
        TextView trainerName, trainerLocation, trainerPhone;
        public TrainerViewHolder(@NonNull View itemView) {
            super(itemView);
            trainerImage = itemView.findViewById(R.id.trainerImage);
            trainerName = itemView.findViewById(R.id.trainerName);
            trainerLocation = itemView.findViewById(R.id.trainerLocation);
            trainerPhone = itemView.findViewById(R.id.trainerPhone);
        }
    }
}
