package com.example.android.xtivity.AdapterMisc;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.xtivity.Model.Product;
import com.example.android.xtivity.R;
import com.example.android.xtivity.Store.ProductActivity;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.StoreViewHolder> {

    Context context;
    List<Product> productList;
    AppData appData;

    public StoreAdapter(Context context, List<Product> productList) {
        this.context = context;
        this.productList = productList;
        appData = new AppData(context);
    }

    @NonNull
    @Override
    public StoreViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.card_store, parent, false);
        return new StoreViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull StoreViewHolder holder, int position) {
        Product prod = productList.get(position);
        Glide.with(context)
                .load(prod.getImageUrl())
                .into(holder.productImage);
        holder.productName.setText(prod.getName());
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class StoreViewHolder extends RecyclerView.ViewHolder {
        ImageView productImage;
        TextView productName;
        public StoreViewHolder(@NonNull View itemView) {
            super(itemView);
            productImage = itemView.findViewById(R.id.prodImage);
            productName = itemView.findViewById(R.id.prodName);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    appData.addStringData(AppData.UID, productList.get(getAdapterPosition()).getUid());
                    context.startActivity(new Intent(context, ProductActivity.class));
                }
            });
        }
    }
}
