package com.example.android.xtivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.android.xtivity.Profile.ProfileActivity;
import com.example.android.xtivity.Settings.SettingsActivity;
import com.example.android.xtivity.Store.StoreActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Set tampilan actionbar
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);

        // Init Bottom Navigation View (yg dibawah)
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_store, R.id.navigation_profile, R.id.navigation_settings)
                .build();
        // Setup Navigation buat Bottom Navigation View
        // Liat list fragmen di res > navigation > mobile_navigation.xml
        final NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        // Listener kalo salah satu tombol di Bottom Nav diklik
        navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        // Kalo home, tampilin fragmen home
                        navController.navigate(R.id.navigation_home);
                        break;
                    case R.id.navigation_store:
                        // kalo store, intent ke activity store
                        startActivity(new Intent(MainActivity.this, StoreActivity.class));
                        break;
                    case R.id.navigation_profile:
                        // kalo profile, intent ke activity profile
                        startActivity(new Intent(MainActivity.this, ProfileActivity.class));
                        break;
                    case R.id.navigation_settings:
                        // kalo setting, intent ke activity setting
                        startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                        break;
                }
                return false;
            }
        });
    }

}
