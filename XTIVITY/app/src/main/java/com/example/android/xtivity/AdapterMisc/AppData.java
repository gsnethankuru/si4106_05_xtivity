package com.example.android.xtivity.AdapterMisc;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.android.xtivity.Model.Transaction;

// helper class buat SharedPreferences
public class AppData {

    Context context;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    // KEY statik buat memudahkan organisasi data
    public static String PASSWORD = "password";
    public static String SPORT = "sport";
    public static String PROD_UID = "productUid";
    public static String PROD_IMAGE = "productImage";
    public static String UID = "uid";
    public static String QTY = "qty";
    public static String TOTAL = "total";
    public static String PROD_NAME = "productName";
    public static String BUYER_NAME = "buyerName";
    public static String BUYER_EMAIL = "buyerEmail";
    public static String BUYER_ADDRESS = "buyerAddress";

    // Konstruktor
    // parameter : Context aplikasi
    public AppData(Context context) {
        this.context = context;
        // Instansiasi SharedPreferences
        // parameter: nama aplikasi, mode SharedPreferences (PRIVATE -> data cuma bisa dibaca aplikasi ini doang)
        sharedPreferences = context.getSharedPreferences("XTIVITY",Context.MODE_PRIVATE);
        // Instansiasi SharedPreferences.Editor, buat nambahin, ngapus, dan overwrite data di SharedPreferences
        editor = sharedPreferences.edit();
    }

    // Tambahin data jenis String
    // parameter: key data, isi data
    public void addStringData(String key, String data) {
        editor.putString(key, data);
        editor.apply();
    }

    // Tambahin data jenis int (integer)
    // parameter: key data, isi data
    public void addIntData(String key, int data) {
        editor.putInt(key, data);
        editor.apply();
    }

    // Instansiasi Model Transaction dari data2 di SharedPreferences
    // parameter: uid transaksi
    public Transaction getTransactionData(String uid) {
        return new Transaction(
                uid,
                sharedPreferences.getString(PROD_UID,""),
                sharedPreferences.getString(PROD_NAME,""),
                sharedPreferences.getInt(QTY,0),
                sharedPreferences.getInt(TOTAL,0),
                sharedPreferences.getString(BUYER_NAME,""),
                sharedPreferences.getString(BUYER_EMAIL,""),
                sharedPreferences.getString(BUYER_ADDRESS,"")
        );
    }

    // Ambil data jenis string
    // parameter: key data
    public String getStringData(String key) {
        return sharedPreferences.getString(key, "");
    }

    // Ambil data jenis int
    // parameter: key data
    public int getIntData(String key) {
        return sharedPreferences.getInt(key,0);
    }
}
