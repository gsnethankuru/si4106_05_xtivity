package com.example.android.xtivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.xtivity.AdapterMisc.AppData;
import com.example.android.xtivity.AdapterMisc.NewsAdapter;
import com.example.android.xtivity.Model.News;
import com.example.android.xtivity.Sports.SportMenuActivity;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    AppData appData;
    DatabaseReference databaseReference;
    NewsAdapter newsAdapter;
    List<String> keyList = new ArrayList<>();
    List<News> newsList = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        // Init var
        databaseReference = FirebaseDatabase.getInstance().getReference();
        appData = new AppData(getActivity());

        // Cara inflate view di fragmen seperti berikut
        // Kalo di activity itu setContentView(R.layout.view);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        // Init view di fragmen

        //Pasang onClickListener ke tiap CardView buat ke Sport
        CardView cvSkate = root.findViewById(R.id.cvSkate);
        cvSkate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // masukin data sport apa yg dipilih ke SharedPreferences
                // data lama (kalo ada) bakal ketiban
                appData.addStringData(AppData.SPORT,SportMenuActivity.SKATE);
                startActivity(new Intent(getActivity(), SportMenuActivity.class));
            }
        });

        CardView cvInline = root.findViewById(R.id.cvInline);
        cvInline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // masukin data sport apa yg dipilih ke SharedPreferences
                // data lama (kalo ada) bakal ketiban
                appData.addStringData(AppData.SPORT,SportMenuActivity.INLINE);
                startActivity(new Intent(getActivity(), SportMenuActivity.class));
            }
        });

        CardView cvBMX = root.findViewById(R.id.cvBMX);
        cvBMX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // masukin data sport apa yg dipilih ke SharedPreferences
                // data lama (kalo ada) bakal ketiban
                appData.addStringData(AppData.SPORT,SportMenuActivity.BMX);
                startActivity(new Intent(getActivity(), SportMenuActivity.class));
            }
        });

        // Init recyclerview
        RecyclerView rvNews = root.findViewById(R.id.rvNews);
        rvNews.setLayoutManager(new LinearLayoutManager(getActivity()));
        newsAdapter = new NewsAdapter(getActivity(),newsList);
        rvNews.setAdapter(newsAdapter);

        return root;
    }

    // Init ChildEventListener Firebase buat ngebaca & mantau data di DB
    // Jadi kalo ada yg ditambah/dihapus,/diedit, bakal langsung keliatan
    // referensi : https://firebase.google.com/docs/database/android/lists-of-data#child-events
    ChildEventListener listener = new ChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            // Kalo ada yg ditambah
            // Tambah uid-nya ke keyList
            keyList.add(dataSnapshot.getKey());
            // Tambah datanya ke model List
            newsList.add(dataSnapshot.getValue(News.class));

            // Kasih tau adapter kalo ada data yg ditambah
            newsAdapter.notifyItemInserted(newsList.size() - 1);
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            // Kalo ada yg diedit
            // cari posisinya berdasarkan uid di keyList
            int index = keyList.indexOf(dataSnapshot.getKey());
            // pake index dari keyList buat ganti data di model List
            newsList.set(index, dataSnapshot.getValue(News.class));

            // Kasih tau adapter kalo ada item yg diedit (posisinya dimana)
            newsAdapter.notifyItemChanged(index);
        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            // Kalo ada yg diapus
            // cari posisinya berdasarkan uid di keyList
            int index = keyList.indexOf(dataSnapshot.getKey());
            // hapus uid dan datanya dari keyList dan model List
            keyList.remove(index);
            newsList.remove(index);

            // Kasih tau adapter ada item yg diapus (posisinya dimana)
            newsAdapter.notifyItemRemoved(index);
        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    @Override
    public void onStart() {
        super.onStart();
        // pasang ChildEventListener pas activity/fragmen onStart
        databaseReference.child("news").addChildEventListener(listener);
    }

    @Override
    public void onStop() {
        super.onStop();
        // lepas ChildEventListener pas activity/fragmen onStart
        // kalo gak dilepas dia bakal terus2an "dengerin" sampe aplikasinya di Destroy
        databaseReference.child("news").removeEventListener(listener);
    }
}
