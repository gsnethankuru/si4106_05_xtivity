package com.example.android.xtivity.Model;

public class Feedback {

    String uid;
    String userUid;
    float rating;
    String additional;

    public Feedback() {
    }

    public Feedback(String uid, String userUid, float rating, String additional) {
        this.uid = uid;
        this.userUid = userUid;
        this.rating = rating;
        this.additional = additional;
    }

    public String getUid() {
        return uid;
    }

    public String getUserUid() {
        return userUid;
    }

    public float getRating() {
        return rating;
    }

    public String getAdditional() {
        return additional;
    }
}
