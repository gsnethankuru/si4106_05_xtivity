package com.example.android.xtivity.Model;

public class User {

    String uid;
    String username;
    String email;
    String phone;
    String imageUrl;
    boolean admin;

    public User() {
    }

    public User(String uid, String username, String email, String phone, String imageUrl, boolean admin) {
        this.uid = uid;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.imageUrl = imageUrl;
        this.admin = admin;
    }

    public String getUid() {
        return uid;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public boolean isAdmin() {
        return admin;
    }
}
