package com.example.android.xtivity.Settings;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RatingBar;

import com.example.android.xtivity.Model.Feedback;
import com.example.android.xtivity.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.sql.Array;
import java.util.List;

public class FeedbackActivity extends AppCompatActivity {

    DatabaseReference databaseReference;
    FirebaseUser mUser;

    RatingBar rating;
    CheckBox cbEasy, cbHelpful, cbFeatures, cbInteresting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        databaseReference = FirebaseDatabase.getInstance().getReference();
        mUser = FirebaseAuth.getInstance().getCurrentUser();

        rating = findViewById(R.id.ratingBar);
        cbEasy = findViewById(R.id.cbEasy);
        cbHelpful = findViewById(R.id.cbHelpful);
        cbFeatures = findViewById(R.id.cbFeatures);
        cbInteresting= findViewById(R.id.cbInteresting);
    }

    public void sendFeedback(View view) {
        String key = databaseReference.child("feedback").push().getKey();
        CheckBox cbArray[] = {cbHelpful, cbEasy, cbFeatures, cbInteresting};
        Feedback feedback = new Feedback(
                key,
                mUser.getUid(),
                rating.getRating(),
                getAdditionalCheckboxes(cbArray)
        );
        databaseReference.child("feedback").child(key).setValue(feedback);
        new AlertDialog.Builder(FeedbackActivity.this)
                .setMessage("Thankyou for rate us, we will improve it")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .show();
    }

    private String getAdditionalCheckboxes(CheckBox cbList[]) {
        String additional = "";
        for (CheckBox cb : cbList) {
            if (cb.isChecked()) {
                additional = additional + cb.getText().toString() + ", ";
            }
        }

        return additional;
    }
}
