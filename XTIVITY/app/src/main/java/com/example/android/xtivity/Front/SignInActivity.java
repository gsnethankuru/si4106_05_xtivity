package com.example.android.xtivity.FrontPages;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.android.xtivity.MainActivity;
import com.example.android.xtivity.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SignInActivity extends AppCompatActivity {

    FirebaseAuth mAuth;
    EditText signinEmail, signinPassword;
    TextView wrongCredential;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        // Init var & view
        mAuth = FirebaseAuth.getInstance();

        signinEmail = findViewById(R.id.signinEmail);
        signinPassword = findViewById(R.id.signinPassword);
        wrongCredential = findViewById(R.id.wrongCredential);
    }

    public void goToRegister(View view) {
        startActivity(new Intent(SignInActivity.this, RegisterActivity.class));
    }

    public void goToForgetPassword(View view) {
        startActivity(new Intent(SignInActivity.this, ForgetPasswordActivity.class));
    }

    public void signin(View view) {
        // Validai kolom email & password
        if (TextUtils.isEmpty(signinEmail.getText())) {
            signinEmail.setError("Required");
        } else if (TextUtils.isEmpty(signinPassword.getText())) {
            signinPassword.setError("Required");
        } else {
            // Kolom email & password tervalidasi
            // SignIn
            mAuth.signInWithEmailAndPassword(signinEmail.getText().toString(), signinPassword.getText().toString())
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Kalo berhasil
                                wrongCredential.setVisibility(View.GONE);
                                startActivity(new Intent(SignInActivity.this, MainActivity.class));
                                finish();
                            } else {
                                // Kalo Gagal
                                wrongCredential.setVisibility(View.VISIBLE);
                            }
                        }
                    });
        }
    }
}
