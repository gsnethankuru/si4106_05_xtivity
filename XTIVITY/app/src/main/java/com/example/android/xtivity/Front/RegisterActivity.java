package com.example.android.xtivity.FrontPages;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.example.android.xtivity.AdapterMisc.AppData;
import com.example.android.xtivity.MainActivity;
import com.example.android.xtivity.Model.User;
import com.example.android.xtivity.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends AppCompatActivity {

    FirebaseAuth mAuth;
    DatabaseReference databaseReference;
    EditText registerUsername, registerEmail, registerPassword, registerPhone;
    AppData appData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Init var & view
        mAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        appData = new AppData(this);

        registerUsername = findViewById(R.id.registerUsername);
        registerEmail = findViewById(R.id.registerEmail);
        registerPassword = findViewById(R.id.registerPassword);
        registerPhone = findViewById(R.id.registerPhone);
    }

    public void goToLogin(View view) {
        startActivity(new Intent(RegisterActivity.this, SignInActivity.class));
    }

    public void register(View view) {
        // Validasi semua kolom
        if (TextUtils.isEmpty(registerEmail.getText())) {
            registerEmail.setError("Required");
        } else if (TextUtils.isEmpty(registerUsername.getText())) {
            registerUsername.setError("Required");
        } else if (TextUtils.isEmpty(registerPassword.getText())) {
            registerPassword.setError("Required");
        } else if (TextUtils.isEmpty(registerPhone.getText())) {
            registerPhone.setError("Required");
        } else if (registerPassword.getText().toString().length() < 8) {
            // Password harus minimal 8 karakter (Dari Firebasenya)
            registerPassword.setError("Minimum 8 characters");
        } else {
            // Semua kolom tervalidasi
            mAuth.createUserWithEmailAndPassword(registerEmail.getText().toString(), registerPassword.getText().toString())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Berhasil daftar
                            // Simpen password user di SharedPreferences
                            // AppData = helper class buat SharedPreferences
                            appData.addStringData(AppData.PASSWORD, registerPassword.getText().toString());

                            // Ambil instance user yg baru dibuat (udah otomatis login)
                            FirebaseUser mUser = mAuth.getCurrentUser();

                            // Instansiasi User pake data yg diinput
                            User user = new User(
                                    mUser.getUid(),
                                    registerUsername.getText().toString(),
                                    registerEmail.getText().toString(),
                                    registerPhone.getText().toString(),
                                    getString(R.string.user_placeholder_image_url),
                                    false
                            );

                            // Simpen data user di DB
                            // child 1 -> nama branch/folder
                            // child 2 -> unique id user
                            databaseReference.child("user").child(mUser.getUid()).setValue(user)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            new AlertDialog.Builder(RegisterActivity.this)
                                                    .setMessage("Your account has been registered")
                                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            dialog.dismiss();
                                                            startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                                                            finish();
                                                        }
                                                    })
                                                    .show();
                                        }
                                    });
                        } else {
                            new AlertDialog.Builder(RegisterActivity.this)
                                    .setMessage("Registration Failed, Please Try Again")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .show();
                        }
                    }
                });
        }
    }
}
