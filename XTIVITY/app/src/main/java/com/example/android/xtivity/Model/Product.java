package com.example.android.xtivity.Model;

public class Product {

    String uid;
    String name;
    int price;
    int qty;
    String imageUrl;

    public Product() {
    }

    public Product(String uid, String name, int price, int qty, String imageUrl) {
        this.uid = uid;
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.imageUrl = imageUrl;
    }

    public String getUid() {
        return uid;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getQty() {
        return qty;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
