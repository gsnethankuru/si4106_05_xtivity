package com.example.android.xtivity.Settings;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.android.xtivity.FrontActivity;
import com.example.android.xtivity.R;
import com.google.firebase.auth.FirebaseAuth;

public class SettingsActivity extends AppCompatActivity {

    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        // Set tampilan actionbar
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Init var
        mAuth = FirebaseAuth.getInstance();
    }

    public void goToReset(View view) {
        startActivity(new Intent(SettingsActivity.this, ResetPasswordActivity.class));
    }

    public void goToFeedback(View view) {
        startActivity(new Intent(SettingsActivity.this, FeedbackActivity.class));
    }

    public void goToAbout(View view) {
        startActivity(new Intent(SettingsActivity.this, AboutActivity.class));
    }

    public void signout(View view) {
        // Sign out
        new AlertDialog.Builder(SettingsActivity.this)
                .setMessage("Sign Out from XTIVITY?")
                .setPositiveButton("Sign Out", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        // Signout dari Firebase
                        // Data di SharedPreferences gak diapus karena bakal di-overwrite pas user login lagi
                        mAuth.signOut();
                        startActivity(new Intent(SettingsActivity.this, FrontActivity.class));
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }
}
