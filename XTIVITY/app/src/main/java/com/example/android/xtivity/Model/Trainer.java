package com.example.android.xtivity.Model;

public class Trainer {

    String uid;
    String name;
    String location;
    String phone;
    String imageUrl;
    String filterTag;

    public Trainer() {
    }

    public Trainer(String uid, String name, String location, String phone, String imageUrl, String filterTag) {
        this.uid = uid;
        this.name = name;
        this.location = location;
        this.phone = phone;
        this.imageUrl = imageUrl;
        this.filterTag = filterTag;
    }

    public String getUid() {
        return uid;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public String getPhone() {
        return phone;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getFilterTag() {
        return filterTag;
    }
}
