package com.example.android.xtivity.AdapterMisc;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.android.xtivity.Model.Tutorial;
import com.example.android.xtivity.R;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class TutorialAdapter extends RecyclerView.Adapter<TutorialAdapter.TutorialViewHolder> {

    Context context;
    List<Tutorial> tutorialList;
    StorageReference storageReference;

    public TutorialAdapter(Context context, List<Tutorial> tutorialList) {
        this.context = context;
        this.tutorialList = tutorialList;
        storageReference = FirebaseStorage.getInstance().getReference();
    }

    @NonNull
    @Override
    public TutorialViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.card_tutorial, parent, false);
        return new TutorialViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TutorialViewHolder holder, int position) {
        Tutorial tutorial = tutorialList.get(position);
        MediaController controller = new MediaController(context);
        controller.setAnchorView(holder.tutorialVideo);
        holder.tutorialVideo.setMediaController(controller);
        holder.tutorialVideo.setVideoURI(Uri.parse(tutorial.getVideoUrl()));
//        holder.downloadVideo(tutorial.getUid());
        holder.tutorialVideo.seekTo(1);
//        SimpleExoPlayer player = new SimpleExoPlayer.Builder(context).build();
//        holder.tutorialVideo.setPlayer(player);
//        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context,
//                Util.getUserAgent(context, "XTIVITY"));
//        MediaSource videoSource =
//                new ProgressiveMediaSource.Factory(dataSourceFactory)
//                        .createMediaSource(Uri.parse(tutorial.getVideoUrl()));
//        player.prepare(videoSource);
        holder.tutorialTitle.setText(tutorial.getTitle());

    }

    @Override
    public int getItemCount() {
        return tutorialList.size();
    }

    public class TutorialViewHolder extends RecyclerView.ViewHolder {
//        PlayerView tutorialVideo;
        VideoView tutorialVideo;
        TextView tutorialTitle;
        public TutorialViewHolder(@NonNull View itemView) {
            super(itemView);
            tutorialVideo = itemView.findViewById(R.id.tutorialVideo);
            tutorialTitle = itemView.findViewById(R.id.tutorialTitle);
        }

//        private void downloadVideo(String uid) {
//            final File file;
//            try {
//                file = File.createTempFile(uid,"mp4");
//                storageReference.child("tutorial").child(uid+".mp4").getFile(file)
//                        .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
//                            @Override
//                            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
//                                tutorialVideo.setVideoPath(file.getAbsolutePath());
//                            }
//                        });
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
    }
}
