package com.example.android.xtivity.Model;

public class Transaction {

    String uid;
    String productUId;
    String productName;
    int qty;
    int total;
    String buyerName;
    String buyerEmail;
    String buyerAddress;

    public Transaction() {
    }

    public Transaction(String uid, String productUId, String productName, int qty, int total, String buyerName, String buyerEmail, String buyerAddress) {
        this.uid = uid;
        this.productUId = productUId;
        this.productName = productName;
        this.qty = qty;
        this.total = total;
        this.buyerName = buyerName;
        this.buyerEmail = buyerEmail;
        this.buyerAddress = buyerAddress;
    }

    public String getUid() {
        return uid;
    }

    public String getProductUId() {
        return productUId;
    }

    public String getProductName() {
        return productName;
    }

    public int getQty() {
        return qty;
    }

    public int getTotal() {
        return total;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public String getBuyerEmail() {
        return buyerEmail;
    }

    public String getBuyerAddress() {
        return buyerAddress;
    }
}
