package com.example.android.xtivity.Sports;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.android.xtivity.AdapterMisc.AppData;
import com.example.android.xtivity.AdapterMisc.TrainerAdapter;
import com.example.android.xtivity.Model.Trainer;
import com.example.android.xtivity.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TrainerActivity extends AppCompatActivity {

    AppData appData;
    DatabaseReference databaseReference;
    RecyclerView rvTrainer;
    TrainerAdapter trainerAdapter;
    List<Trainer> trainerList = new ArrayList<>();
    TextView emptyQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trainer);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        emptyQuery = findViewById(R.id.emptyQuery);
        rvTrainer = findViewById(R.id.rvTrainer);
        rvTrainer.setLayoutManager(new LinearLayoutManager(this));
        trainerAdapter = new TrainerAdapter(this, trainerList);
        rvTrainer.setAdapter(trainerAdapter);

        appData = new AppData(this);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("trainer").orderByChild("filterTag").equalTo(appData.getStringData(AppData.SPORT)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    emptyQuery.setVisibility(View.GONE);
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        trainerList.add(ds.getValue(Trainer.class));
                    }
                    trainerAdapter.notifyDataSetChanged();
                } else {
                    emptyQuery.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
