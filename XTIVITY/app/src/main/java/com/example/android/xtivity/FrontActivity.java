package com.example.android.xtivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.android.xtivity.AdapterMisc.TestData;
import com.example.android.xtivity.FrontPages.SignInActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FrontActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_front);

        TestData.generate();

    }

    public void toSignIn(View view) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            startActivity(new Intent(FrontActivity.this, MainActivity.class));
            finish();
        } else {
            startActivity(new Intent(FrontActivity.this, SignInActivity.class));
            finish();
        }
    }
}
