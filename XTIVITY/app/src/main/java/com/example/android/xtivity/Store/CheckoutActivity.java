package com.example.android.xtivity.Store;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.xtivity.AdapterMisc.AppData;
import com.example.android.xtivity.R;

import java.text.NumberFormat;
import java.util.Locale;

public class CheckoutActivity extends AppCompatActivity {

    AppData appData;

    ImageView checkoutImage;
    TextView checkoutName, checkoutTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        appData = new AppData(this);

        checkoutImage = findViewById(R.id.checkoutDetailImage);
        Glide.with(this)
                .load(Uri.parse(appData.getStringData(AppData.PROD_IMAGE)))
                .into(checkoutImage);
        checkoutName = findViewById(R.id.checkoutName);
        checkoutName.setText(appData.getStringData(AppData.PROD_NAME));
        checkoutTotal = findViewById(R.id.checkoutTotal);
        checkoutTotal.setText(getString(
                R.string.rp,
                NumberFormat.getNumberInstance(Locale.GERMANY).format(appData.getIntData(AppData.TOTAL))
        ));
    }

    public void cancelCheckout(View view) {
        finish();
    }

    public void checkout(View view) {
        startActivity(new Intent(CheckoutActivity.this, CheckoutDetailActivity.class));
    }
}
