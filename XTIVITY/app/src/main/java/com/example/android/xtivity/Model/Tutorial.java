package com.example.android.xtivity.Model;

public class Tutorial {

    String uid;
    String title;
    String videoUrl;
    String filterTag;

    public Tutorial() {
    }

    public Tutorial(String uid, String title, String videoUrl, String filterTag) {
        this.uid = uid;
        this.title = title;
        this.videoUrl = videoUrl;
        this.filterTag = filterTag;
    }

    public String getUid() {
        return uid;
    }

    public String getTitle() {
        return title;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public String getFilterTag() {
        return filterTag;
    }
}
