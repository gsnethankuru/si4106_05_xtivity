package com.example.android.xtivity.Sports;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.android.xtivity.AdapterMisc.AppData;
import com.example.android.xtivity.R;

public class SportMenuActivity extends AppCompatActivity {

    // KEY statik buat di SharedPreferences
    public static String SKATE = "skateboard";
    public static String INLINE = "inline-skate";
    public static String BMX = "bmx";

    String sport;
    TextView sportMenuText;
    RadioGroup rbSportMenu;
    AppData appData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sport_menu);

        // Init var & view
        appData = new AppData(this);

        sportMenuText = findViewById(R.id.sportMenuText);
        // Ambil data sport apa yg dipilih dari SharedPreferences
        sport = appData.getStringData(AppData.SPORT);

        // Sesuaiin background & tulisan header
        switch (sport) {
            case "skateboard":
                sportMenuText.setBackground(getDrawable(R.drawable.skate_menu));
                sportMenuText.setText("Skateboard");
                break;
            case "inline-skate":
                sportMenuText.setBackground(getDrawable(R.drawable.inline_menu));
                sportMenuText.setText("Inline Skate");
                break;
            case "bmx":
                sportMenuText.setBackground(getDrawable(R.drawable.bmx_menu));
                sportMenuText.setText("BMX");
                break;
        }

        rbSportMenu = findViewById(R.id.rbSportMenu);
        // Listener buat nangkep radiobutton apa yg diklik
        rbSportMenu.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rbTutorial:
                        startActivity(new Intent(SportMenuActivity.this, TutorialActivity.class));
                        break;
                    case R.id.rbArena:
                        startActivity(new Intent(SportMenuActivity.this, ArenaActivity.class));
                        break;
                    case R.id.rbEvent:
                        startActivity(new Intent(SportMenuActivity.this, EventActivity.class));
                        break;
                    case R.id.rbTrainer:
                        startActivity(new Intent(SportMenuActivity.this, TrainerActivity.class));
                        break;
                }
            }
        });
    }
}
