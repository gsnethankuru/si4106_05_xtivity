package com.example.android.xtivity.Store;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.xtivity.AdapterMisc.AppData;
import com.example.android.xtivity.Model.Product;
import com.example.android.xtivity.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ProductActivity extends AppCompatActivity {

    DatabaseReference databaseReference;
    AppData appData;
    ImageView productImage;
    TextView productName, productPrice, soldOutQty;
    Spinner spinnerQty;
    int qty = 1;
    List<Integer> spinnerList = new ArrayList<>();
    Button btnBuy;
    ArrayAdapter arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        databaseReference = FirebaseDatabase.getInstance().getReference();
        appData = new AppData(this);

        productImage = findViewById(R.id.productImage);
        productName = findViewById(R.id.productName);
        productPrice = findViewById(R.id.productPrice);
        soldOutQty = findViewById(R.id.soldOutQty);
        spinnerQty = findViewById(R.id.spinnerQty);
        arrayAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, spinnerList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerQty.setAdapter(arrayAdapter);
        spinnerQty.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                qty = position+1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnBuy = findViewById(R.id.btnBuy);

        databaseReference.child("product").child(appData.getStringData(AppData.UID))
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        final Product prod = dataSnapshot.getValue(Product.class);
                        Glide.with(ProductActivity.this)
                                .load(Uri.parse(prod.getImageUrl()))
                                .into(productImage);
                        productName.setText(prod.getName());
                        if (prod.getQty() > 0) {
                            productPrice.setText(
                                    getResources().getString(
                                            R.string.rp,
                                            NumberFormat.getNumberInstance(Locale.GERMANY).format(prod.getPrice())
                                    )
                            );
                            for (int i = 0; i < prod.getQty(); i++) {
                                spinnerList.add(i+1);
                            }
                            arrayAdapter.notifyDataSetChanged();

                            btnBuy.setBackground(getDrawable(R.drawable.button_green));
                            btnBuy.setText("BUY");
                            btnBuy.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    appData.addStringData(AppData.PROD_UID,prod.getUid());
                                    appData.addStringData(AppData.PROD_IMAGE,prod.getImageUrl());
                                    appData.addStringData(AppData.PROD_NAME,prod.getName());
                                    appData.addIntData(AppData.QTY,qty);
                                    appData.addIntData(AppData.TOTAL,qty * prod.getPrice());

                                    startActivity(new Intent(ProductActivity.this, CheckoutActivity.class));
                                }
                            });
                        } else {
                            productPrice.setText(getString(R.string.sold_out));
                            productPrice.setTextColor(getColor(R.color.redColor));
                            spinnerQty.setVisibility(View.GONE);
                            soldOutQty.setVisibility(View.VISIBLE);
                            btnBuy.setBackground(getDrawable(R.drawable.button_red));
                            btnBuy.setText(getString(R.string.sold_out));
                            btnBuy.setClickable(false);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }
}
