package com.example.android.xtivity.Store;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.android.xtivity.AdapterMisc.StoreAdapter;
import com.example.android.xtivity.Model.Product;
import com.example.android.xtivity.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class StoreActivity extends AppCompatActivity {

    DatabaseReference databaseReference;
    RecyclerView rvStore;
    StoreAdapter storeAdapter;
    List<String> keyList = new ArrayList<>();
    List<Product> productList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        databaseReference = FirebaseDatabase.getInstance().getReference();

        rvStore = findViewById(R.id.rvStore);
        rvStore.setLayoutManager(new GridLayoutManager(this, 2));
        storeAdapter = new StoreAdapter(this, productList);
        rvStore.setAdapter(storeAdapter);
    }

    ChildEventListener listener = new ChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            keyList.add(dataSnapshot.getKey());
            productList.add(dataSnapshot.getValue(Product.class));

            storeAdapter.notifyItemInserted(productList.size() - 1);
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            int index = keyList.indexOf(dataSnapshot.getKey());
            productList.set(index,dataSnapshot.getValue(Product.class));

            storeAdapter.notifyItemChanged(index);
        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            int index = keyList.indexOf(dataSnapshot.getKey());
            keyList.remove(index);
            productList.remove(index);

            storeAdapter.notifyItemRemoved(index);
        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        databaseReference.child("product").addChildEventListener(listener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        databaseReference.removeEventListener(listener);
    }
}
