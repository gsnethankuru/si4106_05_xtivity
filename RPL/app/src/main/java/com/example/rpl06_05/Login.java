package com.example.rpl06_05;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }


    public void forgot(View v) {
        Intent  a = new Intent(Login.this, ForgotPassword.class);
        startActivity(a);
    }
}
