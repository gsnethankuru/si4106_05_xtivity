package com.example.rpl06_05;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.rpl06_05.R;

public class ForgotPassword extends AppCompatActivity {
    Button codex;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        codex = (Button) findViewById(R.id.button_code);
        codex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder x = new AlertDialog.Builder(ForgotPassword.this);
                x.setTitle("Code");
                x.setMessage("The Code has Been Sent to Your Email");
                x.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        Toast.makeText(ForgotPassword.this , "Email terkirim" , Toast.LENGTH_SHORT).show();
                    }

                });
                AlertDialog dialog = x.create();
                dialog.show();
            }

        });
    }


}
